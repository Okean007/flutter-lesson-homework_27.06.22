// ignore_for_file: file_names

import 'package:workproject_1/widgets/passwor_text_field.dart';

import 'billBoard_screen.dart';
import 'widgets/login_text_field.dart';
import 'package:flutter/material.dart';

Map account = {'login': 'qwerty', 'password': '123456'};

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final formKey = GlobalKey<FormState>();
  final _loginController = TextEditingController();
  final _passController = TextEditingController();
  @override
  void dispose() {
    _loginController.dispose();
    _passController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24),
        child: Form(
          key: formKey,
        child: Column(
          children: [
            const Spacer(),
            const Text('Вход',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w700,
            ),
            ),
            const SizedBox(
              height: 27,
            ),
            LoginTextField(
              controller: _loginController,
            ),
            const SizedBox(height: 24),
            PasswordTextField(
              controller: _passController,
            ),
            const SizedBox(
              height: 20,
            ),
           ElevatedButton(
             style: ButtonStyle(
               backgroundColor: MaterialStateProperty.all(Colors.red),
               shape: MaterialStateProperty.all(
               RoundedRectangleBorder(
                 borderRadius: BorderRadius.circular(20),
               ),
               ),
               padding: MaterialStateProperty.all(const EdgeInsets.symmetric(
                   horizontal: 30,
                 vertical: 10,
               ),
               ),
             ),
                onPressed: () {
                  formKey.currentState!.validate();
                  if (account['login'] == _loginController.text &&
                      account['password'] == _passController.text) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const Bilboard(),
                      ),
                    );
                  }
                  else {
                    _showMyDialog(context);
                  }
                },
                child: const Text(
                  'Войти',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
           const Spacer(),
          ],
        ),
        ),
      ),
    );
  }
}

Future<void> _showMyDialog(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        title: const Text('Неверный пароль или логин'),
        content: SingleChildScrollView(
          child: ListBody(),
        ),
        actions: <Widget>[
          ElevatedButton(
            child: const Text('close'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}
