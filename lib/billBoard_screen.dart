import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:workproject_1/widgets/Constants/appAssets.dart';
class Bilboard extends StatefulWidget {
  const Bilboard({Key? key}) : super(key: key);

  @override
  State<Bilboard> createState() => _BilboardState();
}

class _BilboardState extends State<Bilboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
    body:Stack(
      children: [
        SizedBox(
          height: MediaQuery.of(context).size.height*0.66,
            width: MediaQuery.of(context).size.width,
            child: Image.asset(AppAssets.images.background1,
            fit: BoxFit.cover,),
        ),
        SizedBox(
          height: MediaQuery.of(context).size.height*0.33,
          width: MediaQuery.of(context).size.width,
          child: Image.asset(AppAssets.images.zastavka,
            fit: BoxFit.cover,),
        ),
         Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children:  [
              const SizedBox(height: 55,
              ),
                const Text('Главная',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.w700,
                    color: Colors.white
                  ),
                ),
              const SizedBox(
                height: 24,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18
                ),
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.symmetric(
                    horizontal: 18,
                    vertical: 12,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8),
                    boxShadow: [
                      BoxShadow(
                        color: const Color(0xff45006F).withOpacity(0.35
                        ),
                        blurRadius: 14,
                      ),
                    ],
                  ),
                  child: ListTile(
                    leading: Container(
                      padding: const EdgeInsets.all(14),
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                        color: Colors.yellow,
                      ),
                      child:  SvgPicture.asset(
                        AppAssets.svg.lingtning,
                      ),
                    ),
                    title:const Text(
                      'Начните зарабатывать',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w700
                      )
                    ),
                    subtitle: const Text('Приобретите тариф Behype-PROи начните свою карьеру!',
                      style: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 44,
              ),
              Expanded(
                child:Container(
                width: double.infinity,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  ),
                  color: Color(0xffF9F8FF),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 18,
                    vertical: 44,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('Категории',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                      ),
                      const SizedBox(height: 20,
                      ),
                      Row(
                        children: [
                      Category(image: AppAssets.images.gettings,
                          title: 'Реклама',
                          subTitle: '106 компаний'),
                      const SizedBox(
                        width: 10,
                      ),
                      Category(image: AppAssets.images.messege,
                          title: 'Взаимопиар',
                          subTitle: '56 Заявок'),
                          const SizedBox(
                            width: 10,
                          ),
                      Category(image: AppAssets.images.like,
                          title: 'Бартер',
                          subTitle: '245 Заявок'),
                        ]
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children:  [
                            const Text('Рекламные кампании',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            SizedBox(
                              height: 24,
                              width: 55,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: const Color(0xffF90640),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                ),
                                  onPressed: (){},
                                  child: const FittedBox(
                                    child: Text('Все',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                              ),
                            ),
                          ],
                        ),
                      const SizedBox(
                        height: 34,
                      ),
                      Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15),
                          ),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 10,
                               color: Color(0xffDED9FF),
                            ),
                          ],
                          gradient: LinearGradient(
                            colors: [
                              Color(0xffD96DFF),
                              Color(0xff6322E0),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        height: 120,
                        width: 170,
                        child: Image.asset(
                          AppAssets.images.frame
                        ),
                      ),
                      Container(
                        height: 38,
                        width: 170,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(15),
                            bottomRight: Radius.circular(15),
                          ),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 10,
                              color: Color(0xffDED9FF),
                            ),
                          ],
                        ),
                        child: const Padding(
                          padding: EdgeInsets.only(top: 2.0),
                          child: Center(
                            child: Text('В новом обновлении',
                              style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              ),
            ],
          ),
      ],
      ),
    );
  }
}
class Category extends StatelessWidget {
  const Category({Key? key, required this.image, required this.title, required this.subTitle}) : super(key: key);
  final String image;
  final String title;
  final String subTitle;
  @override
  Widget build(BuildContext context) {
    return Expanded(child:
      Container(
      padding: const EdgeInsets.all(1
      ),
      decoration: BoxDecoration(
         borderRadius: BorderRadius.circular(15),
        gradient: const LinearGradient(
          colors: [
            Color(0xffF90640),
            Color(0xff8F00FF),
          ],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Container(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            padding: const EdgeInsets.all(13),
            child: Column(
              children: [
                const SizedBox(height: 12
                ),
                Image.asset(image,
                  height: 40,
                ),
                const SizedBox(
                  height: 20,
                ),
                 FittedBox(
                   child: Text(
                    title,
                    style: const TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w700,
                    ),
                ),
                 ),
                const SizedBox(
                    height: 2),
                 Text(
                    subTitle,
                    style: const TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w400
                    )
                ),
              ],
            ),
      ),
    ),
    );
  }
}

