import 'package:flutter/material.dart';



class LoginTextField extends StatelessWidget {
const LoginTextField({Key? key, required this.controller}) : super(key: key);
final TextEditingController controller;
@override
Widget build(BuildContext context) {
  return TextFormField(
    decoration: const InputDecoration(
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(4)),
        borderSide: BorderSide(width: 1,color: Color(0xffD1B5FF)),
      ),
      labelText:'E-mail',
      labelStyle: TextStyle(
        color: Color(0xffD1B5FF),
        fontWeight: FontWeight.w500,
      ),
      border: OutlineInputBorder(
      ),
      fillColor: Color(0xffF9F8FF),
      filled: true,
      counterText: '',
    ),
    maxLength: 8,
    controller: controller,
    validator: (login) {
      if (login == null || login.length < 3) {
        return 'Логин должен содержать не менее 3 символов';
      }
      return null;
    },
  );
}
}