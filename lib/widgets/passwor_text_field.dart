import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:workproject_1/widgets/Constants/appAssets.dart';



class PasswordTextField extends StatelessWidget {
  const PasswordTextField({Key? key, required this.controller}) : super(key: key);
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration:  InputDecoration(
        focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(4)),
          borderSide: BorderSide(width: 1,color: Color(0xffD1B5FF)),
        ),
        labelText:'Password',
        labelStyle: const TextStyle(
          color: Color(0xffD1B5FF),
          fontWeight: FontWeight.w500,
        ),
        border: const OutlineInputBorder(),
        counterText: '',
        fillColor: const Color(0xffF9F8FF),
        filled: true,
        suffixIcon:
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: SvgPicture.asset(
                AppAssets.svg.eye,
                width: 25.0,
              ),
            ),
      ),
      obscureText: true,
      maxLength: 16,
      controller: controller,
      validator: (login) {
        if (login == null || login.length < 6) {
          return 'Пароль должен содержать не менее 6 символов';
        }
        return null;
      },
    );
  }
}
