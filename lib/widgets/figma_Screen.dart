// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:workproject_1/widgets/Constants/appAssets.dart';
import 'package:workproject_1/widgets/Constants/appColors.dart';
import 'package:workproject_1/widgets/Constants/appStyles.dart';

class PhotoScreen extends StatefulWidget {
  const PhotoScreen({Key? key}) : super(key: key);

  @override
  State<PhotoScreen> createState() => _PhotoScreenState();
}

class _PhotoScreenState extends State<PhotoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: AppBar(
        backgroundColor: AppColors.white,
        elevation: 0,
        actions: [
          TextButton(
            onPressed: () {},
            child: const Text(
              'Skip',
              style: AppStyles.nunito16w400,
            ),
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          children: [
            const Spacer(
            ),
            SizedBox(
              width: 270,
              height: 221,
            child: Image.asset(AppAssets.images.noAvatar),),
            const Spacer(
              flex:2,
            ),
            const Text(
              'MyDay team',
              style: AppStyles.nunito40w700,
            ),
            const SizedBox(
              height: 18,
            ),
            Text(
              'prepared for you list of tasks to keep yourself busy and challenged every day, making it more fun and enjoyable',
              textAlign: TextAlign.center,
              style: AppStyles.nunito16w400.copyWith(color: AppColors.black),
            ),
            const Spacer(
              flex: 6,
            ),
          ],
        ),
      ),
    );
  }
}
