abstract class AppAssets {
  static const images = _Images();
  static const svg = _Svg();
}

class _Images {
  const _Images();

  final String noAvatar = 'assets/images/bitmap/MyDay.png';
  final String background1 = 'assets/images/bitmap/background1.png';
  final String background2 = 'assets/images/bitmap/background2.png';
  final String zastavka = 'assets/images/bitmap/zastavka.png';
  final String frame =   'assets/images/bitmap/frame .png';
  final String gettings = 'assets/images/bitmap/gettings.png';
  final String like = 'assets/images/bitmap/like.png';
  final String messege = 'assets/images/bitmap/messege.png';
  final String purpure = 'assets/images/bitmap/purpure.png';
  final String rectangle  = 'assets/images/bitmap/rectangle.png';
  final String window = 'assets/images/bitmap/window.png';
}

class _Svg {
  const _Svg();
  final String lingtning = 'assets/images/svg/lingtning.svg';
  final String eye = 'assets/images/svg/eye.svg';

}
