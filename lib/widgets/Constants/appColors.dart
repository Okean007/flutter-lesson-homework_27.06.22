import 'dart:ui';

class AppColors {
  static const white = Color(0xfff5f5f5);
  static const grey = Color(0xaaA9A7A7);
  static const black = Color(0xff000000);
}
